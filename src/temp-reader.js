const { stat, readdir, readFile } = require('fs').promises;

class TempReader {
  constructor() {
    (async () => {
      this.hasInitError = false;

      if (process.env.MOCK) {
        this.filepath = './mock/temp';
        return;
      }

      try {
        await stat('/sys/bus/w1/devices/');
      } catch (err) {
        this.hasInitError = true;
        return;
      }

      try {
        const files = await readdir('/sys/bus/w1/devices/');
        const driver = files.find(file => file.includes('28-'));

        if (!driver) {
          throw new Error();
        }

        this.filepath = `/sys/bus/w1/devices/${driver}/w1_slave`;
      } catch (err) {
        this.hasInitError = true;
      }
    })();
  }

  get latestValue() {
    return new Promise(async (resolve, reject) => {
      if (!this.filepath) {
        reject('Filepath not set');
      }

      try {
        const fileData = await readFile(this.filepath, 'utf8');
        const temp = fileData.split('t=')[1];
        const celcius = Math.round((temp / 1000) * 10) / 10;
        const fahrenheit = Math.round((celcius * 1.8 + 32) * 10) / 10;
        const date = new Date().getTime();
        resolve({ date, celcius, fahrenheit });
      } catch (err) {
        reject(err);
      }
    });
  }
}

module.exports = TempReader;
