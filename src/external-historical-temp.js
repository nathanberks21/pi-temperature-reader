const request = require('request-promise-native');

const { mkdir, readFile, stat, writeFile } = require('fs').promises;

const API_REQUEST =
  'https://api.darksky.net/forecast/5a035838116f6b0ac5b5513377d180ed/51.75,1.26,';

class ExternalHistoricalTemp {
  constructor() {
    (async () => {
      try {
        await mkdir('./store');
      } catch (err) {}

      try {
        await stat('./store/historical');
      } catch (err) {
        await writeFile('./store/historical', '[]');
      }

      for (let i = 6; i > 0; i--) {
        const dateToCheck = new Date();
        dateToCheck.setDate(dateToCheck.getDate() - i);
        dateToCheck.setHours(0, 0, 0, 0);
        try {
          const file = await readFile('./store/historical', 'utf8');
          await this.checkFileUpToDate(dateToCheck, file);
        } catch (err) {
          console.log(`Error checking external history file ${err}`);
        }
      }
    })();
  }

  checkFileUpToDate(dateToCheck, file) {
    return new Promise(async (resolve, reject) => {
      try {
        const dateMs = dateToCheck.getTime();
        const data = JSON.parse(file);
        const found = data.find(({ epoch }) => epoch === dateMs);

        if (found) {
          return resolve(file);
        }

        if (data.length === 6) {
          data.shift();
        }

        const hourlyTempArr = await this.fetchDateValue(dateMs);
        data.push({ epoch: dateMs, data: hourlyTempArr });
        const stringifiedData = JSON.stringify(data);
        await writeFile('./store/historical', stringifiedData);
        resolve(stringifiedData);
      } catch (err) {
        reject(err);
      }
    });
  }

  getValues() {
    return new Promise(async (resolve, reject) => {
      // As today isn't historical, we need to check if yesterdays values have been added
      const yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);
      yesterday.setHours(0, 0, 0, 0);

      try {
        const file = await readFile('./store/historical', 'utf8');
        const updatedFile = await this.checkFileUpToDate(yesterday, file);
        resolve(JSON.parse(updatedFile));
      } catch (err) {
        reject(err);
      }
    });
  }

  fetchDateValue(dateMs) {
    return new Promise(async (resolve, reject) => {
      const dateSeconds = Math.floor(dateMs / 1000);
      const url = `${API_REQUEST}${dateSeconds}`;
      try {
        const res = await request.get(url);
        const extracted = JSON.parse(res);
        const hourlyTemps = extracted.hourly.data.reduce((acc, d) => {
          const { time, temperature } = d;
          const celcius = ((temperature - 32) * 5) / 9;
          acc.push({ time: time * 1000, celcius });
          return acc;
        }, []);
        resolve(hourlyTemps);
      } catch (err) {
        reject(err);
      }
    });
  }
}

module.exports = ExternalHistoricalTemp;
