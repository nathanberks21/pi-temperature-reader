const { mkdir, readFile, writeFile } = require('fs').promises;

class DataStorage {
  constructor() {
    this.createStore('./store', '/minute');
    this.createStore('./store', '/week');
  }

  async createStore(folder, file) {
    try {
      await mkdir(folder);
    } catch (err) {}

    try {
      await writeFile(`${folder}${file}`, '[]');
    } catch (err) {
      console.log('Error writing store file');
    }
  }

  readStore(file) {
    return new Promise(async (resolve, reject) => {
      try {
        const data = await readFile(file, 'utf8');
        resolve(JSON.parse(data));
      } catch (err) {
        reject(err);
      }
    });
  }

  get minuteData() {
    return new Promise(async (resolve, reject) => {
      try {
        resolve(await this.readStore('./store/minute'));
      } catch (err) {
        reject(err);
      }
    });
  }

  get weekData() {
    return new Promise(async (resolve, reject) => {
      try {
        resolve(await this.readStore('./store/week'));
      } catch (err) {
        reject(err);
      }
    });
  }

  pushMinuteData(d) {
    return new Promise(async (resolve, reject) => {
      try {
        const existingData = await this.minuteData;
        existingData.push(d);

        if (existingData.length > 60) {
          existingData.shift();
        }

        await writeFile('./store/minute', JSON.stringify(existingData));
        resolve();
      } catch (err) {
        reject(`Error pushing minute data ${err}`);
      }
    });
  }

  pushWeekData(d) {
    return new Promise(async (resolve, reject) => {
      try {
        const existingData = await this.weekData;
        existingData.push(d);

        // 10080 minutes in a week
        if (existingData.length > 10080) {
          existingData.shift();
        }

        await writeFile('./store/week', JSON.stringify(existingData));
        resolve();
      } catch (err) {
        reject(`Error pushing week data ${err}`);
      }
    });
  }
}

module.exports = DataStorage;
