const { mkdir, writeFile } = require('fs').promises;

class MockTempWriter {
  constructor(folder, file) {
    (async () => {
      try {
        try {
          await mkdir(folder);
        } catch (err) {}

        this.filepath = `${folder}${file}`;

        // Stagger write time to prevent conflict with file reading
        setTimeout(() => {
          this.writeRandomData();
          setInterval(this.writeRandomData.bind(this), 1000);
        }, 250);
      } catch (err) {
        console.log(`Error creating mock temp writer ${err}`);
      }
    })();
  }

  async writeRandomData() {
    // Random temperature in similar format to expected actual data
    const randomTemperature = (Math.random() * 15 + 20) * 1000;
    const tempString = `t=${randomTemperature}`;
    try {
      await writeFile(this.filepath, tempString);
    } catch (err) {
      console.log(`Error writing mock temp ${err}`);
    }
  }
}

module.exports = MockTempWriter;
