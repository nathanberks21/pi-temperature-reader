const TempReader = require('./temp-reader');

class TempPollController {
  constructor(dataStore) {
    let counter = 0;
    const tempReader = new TempReader();

    setInterval(async () => {
      if (tempReader.hasInitError) {
        console.log('Temperature reader initialisation failed');
      }

      const temp = await tempReader.latestValue;

      try {
        await dataStore.pushMinuteData(temp);
      } catch (err) {
        console.log(`Error pushing minute data ${err}`);
      }

      if (++counter === 60) {
        try {
          await dataStore.pushWeekData(temp);
        } catch (err) {
          console.log(`Error pushing week data ${err}`);
        }

        counter = 0;
      }
    }, 1000);
  }
}

module.exports = TempPollController;
