const express = require('express');
const cors = require('cors');

const ExternalHistoricalTemp = require('./external-historical-temp');
const DataStorage = require('./data-storage');
const MockTempWriter = require('./mock-temp-writer');
const TempPollController = require('./temp-poll-controller');

const app = express().use(cors());
const PORT = 4400;
const dataStore = new DataStorage();
const historicalExternalValues = new ExternalHistoricalTemp();
new TempPollController(dataStore);

if (process.env.MOCK) {
  new MockTempWriter('./mock', '/temp');
}

app.get('/historical', async (_, res) => {
  try {
    const data = await historicalExternalValues.getValues();
    res.json(data);
  } catch (error) {
    res.json({ error });
  }
});

app.get('/minute-data', async (_, res) => {
  try {
    const minuteData = await dataStore.minuteData;
    res.json(minuteData);
  } catch (error) {
    res.json({ error });
  }
});

app.get('/week-data', async (_, res) => {
  try {
    const weekData = await dataStore.weekData;
    res.json(weekData);
  } catch (error) {
    res.json({ error });
  }
});

app.listen(PORT, err =>
  err
    ? console.log(err)
    : console.log(`My Express App listening on port ${PORT}`)
);
